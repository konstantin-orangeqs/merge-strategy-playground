## 0.11.2 (new date)

Another description

### Breaking changes
- Feature 1
- Feature 2
- Feature 3

### Merged branches and closed issues

- Feature 4
- Feature 5

## 0.11.1 (2023-02-07)

### Breaking changes

- Released feature 1

## 0.11.0 (2023-02-03)

Description

### Breaking changes

- Released feature 2
- Released feature 3

### Merged branches and closed issues

- Released feature 4
- Released feature 5
- Released feature 6
- Released feature 7
- Released feature 8
- Released feature 9
